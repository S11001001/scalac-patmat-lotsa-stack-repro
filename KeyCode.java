package test;

enum KeyCode {
  N, DOWN, UP, LEFT, RIGHT, SPACE, ENTER, E, DELETE, I,
  DUMMY1, DUMMY2, DUMMY3, DUMMY4, DUMMY5, DUMMY6, DUMMY7, DUMMY8,
  DUMMY9, DUMMY10, DUMMY11, DUMMY12, DUMMY13, DUMMY14, DUMMY15,
  DUMMY16, DUMMY17, DUMMY18, DUMMY19, DUMMY20, DUMMY21, DUMMY22,
  DUMMY23, DUMMY24, DUMMY25, DUMMY26, DUMMY27, DUMMY28, DUMMY29,
  DUMMY30, DUMMY31, DUMMY32, DUMMY33, DUMMY34, DUMMY35, DUMMY36,
  DUMMY37, DUMMY38, DUMMY39, DUMMY40, DUMMY41, DUMMY42, DUMMY43,
  DUMMY44, DUMMY45, DUMMY46, DUMMY47, DUMMY48, DUMMY49, DUMMY50,
  DUMMY51, DUMMY52, DUMMY53, DUMMY54, DUMMY55, DUMMY56, DUMMY57,
  DUMMY58, DUMMY59, DUMMY60, DUMMY61, DUMMY62, DUMMY63, DUMMY64,
  DUMMY65, DUMMY66, DUMMY67, DUMMY68, DUMMY69, DUMMY70, DUMMY71,
  DUMMY72, DUMMY73, DUMMY74, DUMMY75, DUMMY76, DUMMY77, DUMMY78,
  DUMMY79, DUMMY80, DUMMY81, DUMMY82, DUMMY83, DUMMY84, DUMMY85,
  DUMMY86, DUMMY87, DUMMY88, DUMMY89, DUMMY90, DUMMY91, DUMMY92,
  DUMMY93, DUMMY94, DUMMY95, DUMMY96, DUMMY97, DUMMY98, DUMMY99,
  DUMMY100, DUMMY101, DUMMY102, DUMMY103, DUMMY104, DUMMY105,
  DUMMY106, DUMMY107, DUMMY108, DUMMY109, DUMMY110, DUMMY111,
  DUMMY112, DUMMY113, DUMMY114, DUMMY115, DUMMY116, DUMMY117,
  DUMMY118, DUMMY119, DUMMY120, DUMMY121, DUMMY122, DUMMY123,
  DUMMY124, DUMMY125, DUMMY126, DUMMY127, DUMMY128, DUMMY129,
  DUMMY130, DUMMY131, DUMMY132, DUMMY133, DUMMY134, DUMMY135,
  DUMMY136, DUMMY137, DUMMY138, DUMMY139, DUMMY140, DUMMY141,
  DUMMY142, DUMMY143, DUMMY144, DUMMY145, DUMMY146, DUMMY147,
  DUMMY148, DUMMY149, DUMMY150, DUMMY151, DUMMY152, DUMMY153,
  DUMMY154, DUMMY155, DUMMY156, DUMMY157, DUMMY158, DUMMY159,
  DUMMY160, DUMMY161, DUMMY162, DUMMY163, DUMMY164, DUMMY165,
  DUMMY166, DUMMY167, DUMMY168, DUMMY169, DUMMY170, DUMMY171,
  DUMMY172, DUMMY173, DUMMY174, DUMMY175, DUMMY176, DUMMY177,
  DUMMY178, DUMMY179, DUMMY180, DUMMY181, DUMMY182, DUMMY183,
  DUMMY184, DUMMY185, DUMMY186, DUMMY187, DUMMY188, DUMMY189,
  DUMMY190, DUMMY191, DUMMY192, DUMMY193, DUMMY194, DUMMY195,
  DUMMY196, DUMMY197, DUMMY198, DUMMY199, DUMMY200
}
