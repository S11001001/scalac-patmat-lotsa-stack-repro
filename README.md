# Scalac patmat stack consumption reproduction

This reproduces
[a bug from Ermine Scala](https://bitbucket.org/ermine-language/ermine-scala/issue/23/)
representing a scalac issue upgrading from Scala 2.11.2 to 2.11.5.
Much more detail can be seen at that link.

## The branches herein

Each branch other than `master` demonstrates a change from its parent
that works around the issue, one way or another.  Of special interest
are `fix-by-moving-pipe-case` and `fix-by-expanding-pipe`, which are
practical workarounds should you encounter this bug.

## Scala versions

1. 2.11.6: bad
2. 2.11.5: bad
3. 2.11.4: good
4. 2.12.0-M1: good

## Essential aspects

These features of the reproduction appear to be essential:

1. The 2nd argument to `KeyBinding`
2. Use of | to join two cases
3. `KeyCode` is a large enum
4. The | case isn't the first case
5. There are other cases, of a varying minimum
6. The enum is loaded from classfile, not the Scalac Java parser

The number of match cases needed to trigger the error rises as the
number of values in the enum falls, and vice versa.

## License

The contents of this repository are licensed under
[the 2-clause BSD license](https://tldrlegal.com/license/bsd-2-clause-license-%28freebsd%29),
same license as Ermine Scala.  See `COPYING` for the full copyright
notice.
