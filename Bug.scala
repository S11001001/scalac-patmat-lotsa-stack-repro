package test

import KeyModifier._

case class KeyBinding(val keyCode: KeyCode, val modifier: KeyModifier = NoModifier)

sealed trait KeyModifier

object KeyModifier {
  case object NoModifier extends KeyModifier
  case object AltMod extends KeyModifier
  case object CtrlMod extends KeyModifier
  case object ShiftMod extends KeyModifier
}

object Test {
  def handleKey(kc: KeyCode): Unit =
    KeyBinding(kc) match {
      case KeyBinding(KeyCode.LEFT,CtrlMod)     => ()
      case KeyBinding(KeyCode.SPACE,NoModifier) |
          KeyBinding(KeyCode.ENTER,NoModifier) => ()
      case KeyBinding(KeyCode.I,CtrlMod)        => ()
    }
}
